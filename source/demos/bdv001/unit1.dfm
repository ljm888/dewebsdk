object Form1: TForm1
  Left = 0
  Top = 0
  HelpType = htKeyword
  Margins.Top = 20
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = 'DeWeb DataView 001'
  ClientHeight = 800
  ClientWidth = 1440
  Color = 5647133
  TransparentColorValue = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = 15852561
  Font.Height = -17
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  OnMouseDown = FormMouseDown
  OnMouseUp = FormMouseUp
  PixelsPerInch = 96
  TextHeight = 23
  object P_0: TPanel
    AlignWithMargins = True
    Left = 30
    Top = 10
    Width = 1380
    Height = 150
    HelpType = htKeyword
    HelpContext = 5
    Margins.Left = 30
    Margins.Top = 10
    Margins.Right = 30
    Align = alTop
    Color = clNone
    ParentBackground = False
    TabOrder = 0
    object Panel8: TPanel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 300
      Height = 142
      Hint = '{"dwstyle":"border:solid 2px #178BA5;","radius":"3px"}'
      HelpType = htKeyword
      HelpContext = 5
      Align = alLeft
      Color = clNone
      ParentBackground = False
      TabOrder = 0
      object Panel9: TPanel
        Left = 1
        Top = 1
        Width = 298
        Height = 32
        Hint = '{'#13#10#9'"dwstyle": "border-bottom:solid 1px #178BA5;"'#13#10'}'
        Align = alTop
        Color = 6501661
        ParentBackground = False
        TabOrder = 0
        object Label14: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 290
          Height = 22
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          Caption = #20844#24320#25968#25454#23637#31034
          Font.Charset = ANSI_CHARSET
          Font.Color = 15852561
          Font.Height = -17
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 102
          ExplicitHeight = 23
        end
      end
      object Panel10: TPanel
        Left = 1
        Top = 33
        Width = 298
        Height = 54
        Hint = '{"dwstyle":"border-bottom:solid 1px #178BA5;"}'
        HelpType = htKeyword
        HelpContext = 5
        Align = alTop
        Color = clNone
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        object L_000: TLabel
          AlignWithMargins = True
          Left = 7
          Top = 4
          Width = 90
          Height = 46
          Margins.Left = 6
          Align = alLeft
          Alignment = taCenter
          AutoSize = False
          Caption = '500'#13#20826#21153#20844#24320
          Color = 533252151
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
        end
        object L_002: TLabel
          AlignWithMargins = True
          Left = 201
          Top = 4
          Width = 90
          Height = 46
          Margins.Right = 6
          Align = alRight
          Alignment = taCenter
          AutoSize = False
          Caption = '400'#13#36130#21153#20844#24320
          Color = 536383365
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitLeft = 195
        end
        object L_001: TLabel
          AlignWithMargins = True
          Left = 103
          Top = 4
          Width = 92
          Height = 46
          Align = alClient
          Alignment = taCenter
          AutoSize = False
          Caption = '300'#13#25919#21153#20844#24320
          Color = 529058329
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitLeft = 106
          ExplicitWidth = 96
          ExplicitHeight = 48
        end
      end
      object Panel11: TPanel
        Left = 1
        Top = 87
        Width = 298
        Height = 54
        Hint = '{"dwstyle":"border-bottom:solid 1px #178BA5;"}'
        HelpType = htKeyword
        HelpContext = 5
        Align = alClient
        Color = clNone
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        object L_003: TLabel
          AlignWithMargins = True
          Left = 7
          Top = 4
          Width = 68
          Height = 44
          Margins.Left = 6
          Margins.Bottom = 5
          Align = alLeft
          Alignment = taCenter
          AutoSize = False
          Caption = '363'#13#26449#21153#20844#24320
          Color = 523473405
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitLeft = 4
          ExplicitHeight = 46
        end
        object L_006: TLabel
          AlignWithMargins = True
          Left = 224
          Top = 4
          Width = 67
          Height = 44
          Margins.Right = 6
          Margins.Bottom = 5
          Align = alRight
          Alignment = taCenter
          AutoSize = False
          Caption = '137'#13#26435#21147#20844#24320
          Color = 521033974
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitHeight = 46
        end
        object L_004: TLabel
          AlignWithMargins = True
          Left = 81
          Top = 4
          Width = 63
          Height = 44
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          AutoSize = False
          Caption = '216'#13#23621#21153#20844#24320
          Color = 521064950
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitLeft = 77
          ExplicitWidth = 67
          ExplicitHeight = 46
        end
        object L_005: TLabel
          AlignWithMargins = True
          Left = 150
          Top = 4
          Width = 68
          Height = 44
          Margins.Bottom = 5
          Align = alRight
          Alignment = taCenter
          AutoSize = False
          Caption = '90'#13#20538#21153#20844#24320
          Color = 536841262
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitLeft = 154
          ExplicitHeight = 46
        end
      end
    end
    object Panel12: TPanel
      AlignWithMargins = True
      Left = 1076
      Top = 4
      Width = 300
      Height = 142
      Hint = '{"dwstyle":"border:solid 2px #178BA5;","radius":"3px"}'
      HelpType = htKeyword
      HelpContext = 5
      Align = alRight
      Color = clNone
      ParentBackground = False
      TabOrder = 1
      object Panel13: TPanel
        Left = 1
        Top = 1
        Width = 298
        Height = 32
        Hint = '{"dwstyle":"border-bottom:solid 1px #178BA5;"}'
        Align = alTop
        Color = 6501661
        ParentBackground = False
        TabOrder = 0
        object Label22: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 290
          Height = 22
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          Caption = #32479#35745#25968#25454#23637#31034
          Font.Charset = ANSI_CHARSET
          Font.Color = 15852561
          Font.Height = -17
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 102
          ExplicitHeight = 23
        end
      end
      object Panel14: TPanel
        Left = 1
        Top = 33
        Width = 298
        Height = 54
        Hint = '{"dwstyle":"border-bottom:solid 1px #178BA5;"}'
        HelpType = htKeyword
        HelpContext = 5
        Align = alTop
        Color = clNone
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        object L_020: TLabel
          AlignWithMargins = True
          Left = 7
          Top = 4
          Width = 139
          Height = 46
          Margins.Left = 6
          Align = alLeft
          Alignment = taCenter
          AutoSize = False
          Caption = '3521'#13#26412#24180#20844#24320
          Color = 533252151
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
        end
        object L_021: TLabel
          AlignWithMargins = True
          Left = 152
          Top = 4
          Width = 139
          Height = 46
          Margins.Right = 6
          Align = alClient
          Alignment = taCenter
          AutoSize = False
          Caption = '423'#13#26412#26376#20844#24320
          Color = 536383365
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitLeft = 109
          ExplicitWidth = 140
        end
      end
      object Panel15: TPanel
        Left = 1
        Top = 87
        Width = 298
        Height = 54
        Hint = '{"dwstyle":"border-bottom:solid 1px #178BA5;"}'
        HelpType = htKeyword
        HelpContext = 5
        Align = alClient
        Color = clNone
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        object L_022: TLabel
          AlignWithMargins = True
          Left = 7
          Top = 4
          Width = 90
          Height = 44
          Margins.Left = 6
          Margins.Bottom = 5
          Align = alLeft
          Alignment = taCenter
          AutoSize = False
          Caption = '2306'#13#30005#21147#25968#25454
          Color = 523473405
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitHeight = 46
        end
        object L_024: TLabel
          AlignWithMargins = True
          Left = 201
          Top = 4
          Width = 90
          Height = 44
          Margins.Right = 6
          Margins.Bottom = 5
          Align = alRight
          Alignment = taCenter
          AutoSize = False
          Caption = '387'#13#25490#27745#25968#25454
          Color = 521033974
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitLeft = 224
          ExplicitHeight = 46
        end
        object L_023: TLabel
          AlignWithMargins = True
          Left = 103
          Top = 4
          Width = 92
          Height = 44
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          AutoSize = False
          Caption = '238'#13#29992#27700#25968#25454
          Color = 521064950
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitLeft = 77
          ExplicitWidth = 67
          ExplicitHeight = 46
        end
      end
    end
    object Panel16: TPanel
      Left = 307
      Top = 1
      Width = 766
      Height = 148
      HelpType = htKeyword
      HelpContext = 5
      Align = alClient
      Color = clNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 2
      object Panel17: TPanel
        Left = 1
        Top = 1
        Width = 764
        Height = 60
        HelpType = htKeyword
        HelpContext = 5
        Align = alTop
        Color = clNone
        ParentBackground = False
        TabOrder = 0
        object Label25: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 1
          Width = 756
          Height = 53
          Margins.Top = 0
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          Caption = #26381#21153#22823#25968#25454#21487#35270#21270#30417#31649#24179#21488
          Font.Charset = ANSI_CHARSET
          Font.Color = 15852561
          Font.Height = -36
          Font.Name = #24494#36719#38597#40657
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 432
          ExplicitHeight = 47
        end
        object Image1: TImage
          Left = 1
          Top = 1
          Width = 762
          Height = 58
          Hint = '{"src":"image/bdv/title_bg1.png"}'
          Align = alClient
          Stretch = True
          ExplicitLeft = 384
          ExplicitTop = 8
          ExplicitWidth = 105
          ExplicitHeight = 105
        end
      end
      object Panel1: TPanel
        Left = 1
        Top = 61
        Width = 764
        Height = 86
        HelpType = htKeyword
        HelpContext = 5
        Align = alClient
        Color = clNone
        ParentBackground = False
        TabOrder = 1
        object Label1: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 1
          Width = 245
          Height = 79
          Margins.Top = 0
          Margins.Right = 30
          Margins.Bottom = 5
          Align = alLeft
          Alignment = taRightJustify
          AutoSize = False
          Caption = #20844#24320#26381#21153#25968#37327
          Font.Charset = ANSI_CHARSET
          Font.Color = 15852561
          Font.Height = -36
          Font.Name = #24494#36719#38597#40657
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
        end
        object Panel2: TPanel
          AlignWithMargins = True
          Left = 593
          Top = 4
          Width = 70
          Height = 78
          HelpKeyword = 'dvbox'
          HelpContext = 12
          Align = alLeft
          Color = 526595357
          ParentBackground = False
          TabOrder = 0
          object L_014: TLabel
            AlignWithMargins = True
            Left = 11
            Top = 4
            Width = 48
            Height = 70
            Margins.Left = 10
            Margins.Right = 10
            Align = alClient
            Alignment = taCenter
            Caption = '8'
            Font.Charset = ANSI_CHARSET
            Font.Color = 15852561
            Font.Height = -49
            Font.Name = #24494#36719#38597#40657
            Font.Style = [fsBold]
            ParentFont = False
            Layout = tlCenter
            ExplicitWidth = 30
            ExplicitHeight = 65
          end
        end
        object Panel3: TPanel
          AlignWithMargins = True
          Left = 517
          Top = 4
          Width = 70
          Height = 78
          HelpKeyword = 'dvbox'
          HelpContext = 12
          Align = alLeft
          Color = 526595357
          ParentBackground = False
          TabOrder = 1
          object L_013: TLabel
            AlignWithMargins = True
            Left = 11
            Top = 4
            Width = 48
            Height = 70
            Margins.Left = 10
            Margins.Right = 10
            Align = alClient
            Alignment = taCenter
            Caption = '0'
            Font.Charset = ANSI_CHARSET
            Font.Color = 15852561
            Font.Height = -49
            Font.Name = #24494#36719#38597#40657
            Font.Style = [fsBold]
            ParentFont = False
            Layout = tlCenter
            ExplicitWidth = 30
            ExplicitHeight = 65
          end
        end
        object Panel4: TPanel
          AlignWithMargins = True
          Left = 441
          Top = 4
          Width = 70
          Height = 78
          HelpKeyword = 'dvbox'
          HelpContext = 12
          Align = alLeft
          Color = 526595357
          ParentBackground = False
          TabOrder = 2
          object L_012: TLabel
            AlignWithMargins = True
            Left = 11
            Top = 4
            Width = 48
            Height = 70
            Margins.Left = 10
            Margins.Right = 10
            Align = alClient
            Alignment = taCenter
            Caption = '5'
            Font.Charset = ANSI_CHARSET
            Font.Color = 15852561
            Font.Height = -49
            Font.Name = #24494#36719#38597#40657
            Font.Style = [fsBold]
            ParentFont = False
            Layout = tlCenter
            ExplicitWidth = 30
            ExplicitHeight = 65
          end
        end
        object Panel5: TPanel
          AlignWithMargins = True
          Left = 358
          Top = 4
          Width = 70
          Height = 78
          HelpKeyword = 'dvbox'
          HelpContext = 12
          Margins.Right = 10
          Align = alLeft
          Color = 526595357
          ParentBackground = False
          TabOrder = 3
          object L_011: TLabel
            AlignWithMargins = True
            Left = 11
            Top = 4
            Width = 48
            Height = 70
            Margins.Left = 10
            Margins.Right = 10
            Align = alClient
            Alignment = taCenter
            Caption = '9'
            Font.Charset = ANSI_CHARSET
            Font.Color = 15852561
            Font.Height = -49
            Font.Name = #24494#36719#38597#40657
            Font.Style = [fsBold]
            ParentFont = False
            Layout = tlCenter
            ExplicitWidth = 30
            ExplicitHeight = 65
          end
        end
        object Panel6: TPanel
          AlignWithMargins = True
          Left = 282
          Top = 4
          Width = 70
          Height = 78
          HelpKeyword = 'dvbox'
          HelpContext = 12
          Align = alLeft
          Color = 526595357
          ParentBackground = False
          TabOrder = 4
          object L_010: TLabel
            AlignWithMargins = True
            Left = 11
            Top = 4
            Width = 48
            Height = 70
            Margins.Left = 10
            Margins.Right = 10
            Align = alClient
            Alignment = taCenter
            Caption = '6'
            Font.Charset = ANSI_CHARSET
            Font.Color = 15852561
            Font.Height = -49
            Font.Name = #24494#36719#38597#40657
            Font.Style = [fsBold]
            ParentFont = False
            Layout = tlCenter
            ExplicitWidth = 30
            ExplicitHeight = 65
          end
        end
      end
    end
  end
  object Panel7: TPanel
    AlignWithMargins = True
    Left = 30
    Top = 166
    Width = 1380
    Height = 169
    HelpType = htKeyword
    HelpContext = 5
    Margins.Left = 30
    Margins.Right = 30
    Align = alTop
    Color = clNone
    ParentBackground = False
    TabOrder = 1
    object Panel18: TPanel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 300
      Height = 161
      Hint = '{"dwstyle":"border:solid 2px #178BA5;","radius":"3px"}'
      HelpType = htKeyword
      HelpContext = 5
      Align = alLeft
      Color = clNone
      ParentBackground = False
      TabOrder = 0
      object Panel19: TPanel
        Left = 1
        Top = 1
        Width = 298
        Height = 32
        Hint = '{"dwstyle":"border-bottom:solid 1px #178BA5;"}'
        Align = alTop
        Color = 6501661
        ParentBackground = False
        TabOrder = 0
        object Label7: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 290
          Height = 22
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          Caption = #21508#33258#28982#26449#26381#21153#24773#20917#25968#25454
          Font.Charset = ANSI_CHARSET
          Font.Color = 15852561
          Font.Height = -17
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 170
          ExplicitHeight = 23
        end
      end
      object SG_10: TStringGrid
        AlignWithMargins = True
        Left = 31
        Top = 38
        Width = 228
        Height = 122
        HelpType = htKeyword
        HelpKeyword = 'dvcapsule'
        Margins.Left = 30
        Margins.Top = 5
        Margins.Right = 40
        Margins.Bottom = 0
        Align = alClient
        TabOrder = 1
      end
    end
    object Panel22: TPanel
      AlignWithMargins = True
      Left = 1076
      Top = 4
      Width = 300
      Height = 161
      Hint = '{"dwstyle":"border:solid 2px #178BA5;","radius":"3px"}'
      HelpType = htKeyword
      HelpContext = 5
      Align = alRight
      Color = clNone
      ParentBackground = False
      TabOrder = 1
      object Panel23: TPanel
        Left = 1
        Top = 1
        Width = 298
        Height = 32
        Hint = '{"dwstyle":"border-bottom:solid 1px #178BA5;"}'
        Align = alTop
        Color = 6501661
        ParentBackground = False
        TabOrder = 0
        object Label30: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 290
          Height = 22
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          Caption = #24453#20844#24320#25968#25454
          Font.Charset = ANSI_CHARSET
          Font.Color = 15852561
          Font.Height = -17
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 85
          ExplicitHeight = 23
        end
      end
      object Panel24: TPanel
        Left = 1
        Top = 33
        Width = 298
        Height = 54
        Hint = '{"dwstyle":"border-bottom:solid 1px #178BA5;"}'
        HelpType = htKeyword
        HelpContext = 5
        Align = alTop
        Color = clNone
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        object L_130: TLabel
          AlignWithMargins = True
          Left = 7
          Top = 4
          Width = 139
          Height = 46
          Margins.Left = 6
          Align = alLeft
          Alignment = taCenter
          AutoSize = False
          Caption = '3521'#13#26412#24180#20844#24320
          Color = 533252151
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitTop = 2
        end
        object L_131: TLabel
          AlignWithMargins = True
          Left = 152
          Top = 4
          Width = 139
          Height = 46
          Margins.Right = 6
          Align = alClient
          Alignment = taCenter
          AutoSize = False
          Caption = '423'#13#26412#26376#20844#24320
          Color = 536383365
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitLeft = 109
          ExplicitWidth = 140
        end
      end
      object Panel25: TPanel
        Left = 1
        Top = 87
        Width = 298
        Height = 73
        Hint = '{"dwstyle":"border-bottom:solid 1px #178BA5;"}'
        HelpType = htKeyword
        HelpContext = 5
        Align = alClient
        Color = clNone
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        object L_132: TLabel
          AlignWithMargins = True
          Left = 7
          Top = 4
          Width = 90
          Height = 63
          Margins.Left = 6
          Margins.Bottom = 5
          Align = alLeft
          Alignment = taCenter
          AutoSize = False
          Caption = '2306'#13#30005#21147#25968#25454
          Color = 523473405
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitHeight = 46
        end
        object L_134: TLabel
          AlignWithMargins = True
          Left = 201
          Top = 4
          Width = 90
          Height = 63
          Margins.Right = 6
          Margins.Bottom = 5
          Align = alRight
          Alignment = taCenter
          AutoSize = False
          Caption = '387'#13#25490#27745#25968#25454
          Color = 521033974
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitLeft = 224
          ExplicitHeight = 46
        end
        object L_133: TLabel
          AlignWithMargins = True
          Left = 103
          Top = 4
          Width = 92
          Height = 63
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          AutoSize = False
          Caption = '238'#13#29992#27700#25968#25454
          Color = 521064950
          ParentColor = False
          Transparent = False
          Layout = tlCenter
          WordWrap = True
          ExplicitLeft = 77
          ExplicitWidth = 67
          ExplicitHeight = 46
        end
      end
    end
    object Panel26: TPanel
      Left = 307
      Top = 1
      Width = 766
      Height = 167
      HelpType = htKeyword
      HelpContext = 5
      Align = alClient
      Color = clNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 2
      object Panel27: TPanel
        AlignWithMargins = True
        Left = 4
        Top = 4
        Width = 360
        Height = 159
        HelpType = htKeyword
        HelpContext = 5
        Align = alLeft
        Color = clNone
        ParentBackground = False
        TabOrder = 0
        object M_11: TMemo
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 352
          Height = 151
          HelpType = htKeyword
          HelpKeyword = 'echarts'
          Align = alClient
          Lines.Strings = (
            'this.value11 =  ['
            '        { value: 1048, name: '#39#22823#33829#26449#39' },'
            '        { value: 735, name: '#39#25955#27954#26449#39' },'
            '        { value: 580, name: '#39#24247#28286#26449#39' },'
            '        { value: 484, name: '#39#28540#21475#26449#39' },'
            '        { value: 300, name: '#39#37101#27827#26449#39' }'
            '      ];'
            '//====='
            'option = {'
            'animation: false,'
            '  tooltip: {'
            '    trigger: '#39'item'#39
            '  },'
            '  legend: {'
            '    top: '#39'center'#39','
            '    left: '#39'70%'#39','
            '    orient:"vertical",'
            '    textStyle:{'
            '      color:"#fff"'
            '    }'
            '   '
            '  },'
            '  series: ['
            '    {'
            '      name: '#39'Access From'#39','
            '      type: '#39'pie'#39','
            '      radius: ['#39'40%'#39', '#39'70%'#39'],'
            '      avoidLabelOverlap: false,'
            '      itemStyle: {'
            '        borderRadius: 0,'
            '        borderColor: '#39'#fff'#39','
            '        borderWidth: 2'
            '      },'
            '      label: {'
            '        show: false,'
            '        position: '#39'center'#39
            '      },'
            '      emphasis: {'
            '        label: {'
            '          show: true,'
            '          fontSize: 40,'
            '          fontWeight: '#39'bold'#39
            '        }'
            '      },'
            '      labelLine: {'
            '        show: false'
            '      },'
            '      data:this.value11'
            '    }'
            '  ]'
            '};')
          ScrollBars = ssBoth
          TabOrder = 0
        end
      end
      object Panel28: TPanel
        AlignWithMargins = True
        Left = 370
        Top = 4
        Width = 392
        Height = 159
        HelpType = htKeyword
        HelpContext = 5
        Align = alClient
        Color = clNone
        ParentBackground = False
        TabOrder = 1
        object M_12: TMemo
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 384
          Height = 151
          HelpType = htKeyword
          HelpKeyword = 'echarts'
          Align = alClient
          Lines.Strings = (
            'this.value12 = ['
            '        { value: 40, name: '#39#24352#19996#39' },'
            '        { value: 30, name: '#39#38472#35779#39' },'
            '        { value: 28, name: '#39#24352#35199#39' },'
            '        { value: 20, name: '#39#26611#26519#26725#39' },'
            '        { value: 22, name: '#39#40644#26725#39' }'
            '      ];'
            '//====='
            'option = {'
            '  legend: {'
            '    top: '#39'center'#39','
            '    left:'#39'80%'#39','
            '    orient:'#39'vertical'#39','
            '    textStyle:{'
            '      color:'#39'#fff'#39
            '    }'
            '  },'
            '  '
            '  series: ['
            '    {'
            '      type: '#39'pie'#39','
            '      radius: [10, 35],'
            '      center: ['#39'50%'#39', '#39'50%'#39'],'
            '      roseType: '#39'area'#39','
            '      itemStyle: {'
            '        borderRadius: 0'
            '      },'
            '      data: this.value12'
            '    }'
            '  ]'
            '};')
          ScrollBars = ssBoth
          TabOrder = 0
        end
      end
    end
  end
  object Panel20: TPanel
    AlignWithMargins = True
    Left = 30
    Top = 341
    Width = 1380
    Height = 56
    HelpType = htKeyword
    HelpContext = 5
    Margins.Left = 30
    Margins.Right = 30
    Align = alTop
    Color = clNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -12
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    object Panel21: TPanel
      AlignWithMargins = True
      Left = 706
      Top = 4
      Width = 318
      Height = 48
      Hint = 
        '{"dwstyle":"background-image:radial-gradient(#165280, #1380A2);b' +
        'order:solid 2px #CDDBEA; box-shadow: 0px 0px 5px #fff;","radius"' +
        ':"3px"}'
      HelpType = htKeyword
      HelpContext = 12
      Margins.Right = 30
      Align = alLeft
      Color = 526595357
      ParentBackground = False
      TabOrder = 0
      object L_22: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 3
        Width = 296
        Height = 39
        Margins.Left = 10
        Margins.Top = 2
        Margins.Right = 10
        Margins.Bottom = 5
        Align = alClient
        Alignment = taCenter
        Caption = '18'
        Font.Charset = ANSI_CHARSET
        Font.Color = 15852561
        Font.Height = -33
        Font.Name = #24494#36719#38597#40657
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
        ExplicitWidth = 40
        ExplicitHeight = 44
      end
      object Label9: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 0
        Width = 90
        Height = 31
        Margins.Left = 10
        Margins.Right = 10
        Alignment = taCenter
        AutoSize = False
        Caption = #26381#21153#22320#28857
        Font.Charset = ANSI_CHARSET
        Font.Color = 15852561
        Font.Height = -17
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
      end
    end
    object Panel29: TPanel
      AlignWithMargins = True
      Left = 1057
      Top = 4
      Width = 319
      Height = 48
      Hint = 
        '{"dwstyle":"background-image:radial-gradient(#165280, #1380A2);b' +
        'order:solid 2px #CDDBEA; box-shadow: 0px 0px 5px #fff;","radius"' +
        ':"3px"}'
      HelpType = htKeyword
      HelpContext = 12
      Align = alClient
      Color = 526595357
      ParentBackground = False
      TabOrder = 1
      object L_23: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 3
        Width = 297
        Height = 39
        Margins.Left = 10
        Margins.Top = 2
        Margins.Right = 10
        Margins.Bottom = 5
        Align = alClient
        Alignment = taCenter
        Caption = '6'
        Font.Charset = ANSI_CHARSET
        Font.Color = 15852561
        Font.Height = -33
        Font.Name = #24494#36719#38597#40657
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
        ExplicitWidth = 20
        ExplicitHeight = 44
      end
      object Label11: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 0
        Width = 65
        Height = 31
        Margins.Left = 10
        Margins.Right = 10
        Alignment = taCenter
        AutoSize = False
        Caption = #26381#21153#31867#22411
        Font.Charset = ANSI_CHARSET
        Font.Color = 15852561
        Font.Height = -17
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
      end
    end
    object Panel30: TPanel
      AlignWithMargins = True
      Left = 355
      Top = 4
      Width = 318
      Height = 48
      Hint = 
        '{"dwstyle":"background-image:radial-gradient(#165280, #1380A2);b' +
        'order:solid 2px #CDDBEA; box-shadow: 0px 0px 5px #fff;","radius"' +
        ':"3px"}'
      HelpType = htKeyword
      HelpContext = 12
      Margins.Right = 30
      Align = alLeft
      Color = 526595357
      ParentBackground = False
      TabOrder = 2
      object L_21: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 3
        Width = 296
        Height = 39
        Margins.Left = 10
        Margins.Top = 2
        Margins.Right = 10
        Margins.Bottom = 5
        Align = alClient
        Alignment = taCenter
        Caption = '132'
        Font.Charset = ANSI_CHARSET
        Font.Color = 15852561
        Font.Height = -33
        Font.Name = #24494#36719#38597#40657
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
        ExplicitWidth = 60
        ExplicitHeight = 44
      end
      object Label13: TLabel
        AlignWithMargins = True
        Left = 7
        Top = -4
        Width = 90
        Height = 31
        Margins.Left = 10
        Margins.Right = 10
        Alignment = taCenter
        AutoSize = False
        Caption = #26381#21153#39033#30446
        Font.Charset = ANSI_CHARSET
        Font.Color = 15852561
        Font.Height = -17
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
      end
    end
    object Panel31: TPanel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 318
      Height = 48
      Hint = 
        '{"dwstyle":"background-image:radial-gradient(#165280, #1380A2);b' +
        'order:solid 2px #CDDBEA; box-shadow: 0px 0px 5px #fff;","radius"' +
        ':"3px"}'
      HelpType = htKeyword
      HelpContext = 12
      Margins.Right = 30
      Align = alLeft
      Color = 526595357
      ParentBackground = False
      TabOrder = 3
      object L_20: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 3
        Width = 296
        Height = 39
        Margins.Left = 10
        Margins.Top = 2
        Margins.Right = 10
        Margins.Bottom = 5
        Align = alClient
        Alignment = taCenter
        Caption = '342'
        Font.Charset = ANSI_CHARSET
        Font.Color = 15852561
        Font.Height = -33
        Font.Name = #24494#36719#38597#40657
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
        ExplicitWidth = 60
        ExplicitHeight = 44
      end
      object Label36: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 0
        Width = 90
        Height = 31
        Margins.Left = 10
        Margins.Right = 10
        Alignment = taCenter
        AutoSize = False
        Caption = #26381#21153#23545#35937
        Font.Charset = ANSI_CHARSET
        Font.Color = 15852561
        Font.Height = -17
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
      end
    end
  end
  object Panel32: TPanel
    AlignWithMargins = True
    Left = 30
    Top = 403
    Width = 1380
    Height = 180
    HelpType = htKeyword
    HelpContext = 5
    Margins.Left = 30
    Margins.Right = 30
    Align = alTop
    Color = clNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -12
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    object Panel34: TPanel
      AlignWithMargins = True
      Left = 941
      Top = 4
      Width = 435
      Height = 172
      HelpKeyword = 'dvbox'
      HelpContext = 13
      Align = alRight
      Color = clNone
      ParentBackground = False
      TabOrder = 0
      object Panel40: TPanel
        AlignWithMargins = True
        Left = 21
        Top = 26
        Width = 143
        Height = 32
        Hint = '{"radius":"3px","dwstyle":"border-left:double 16px #19CA88;"}'
        Margins.Left = 20
        Margins.Top = 25
        Margins.Right = 270
        Align = alTop
        Alignment = taLeftJustify
        Color = clNone
        ParentBackground = False
        TabOrder = 0
        object Label44: TLabel
          AlignWithMargins = True
          Left = 6
          Top = 1
          Width = 136
          Height = 30
          Margins.Left = 5
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alClient
          Alignment = taCenter
          Caption = #26381#21153#28909#28857
          Color = 13924904
          Font.Charset = ANSI_CHARSET
          Font.Color = 15852561
          Font.Height = -17
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
          Layout = tlCenter
          ExplicitWidth = 68
          ExplicitHeight = 23
        end
      end
      object Panel41: TPanel
        AlignWithMargins = True
        Left = 21
        Top = 93
        Width = 393
        Height = 32
        Margins.Left = 20
        Margins.Top = 0
        Margins.Right = 20
        Margins.Bottom = 0
        Align = alTop
        Color = clNone
        ParentBackground = False
        TabOrder = 1
        object L_322: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 299
          Height = 24
          Align = alClient
          Caption = #21508#33258#28982#26449#26381#21153#24773#20917#25968#25454
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 130
          ExplicitHeight = 19
        end
        object L_323: TLabel
          AlignWithMargins = True
          Left = 309
          Top = 4
          Width = 80
          Height = 24
          Align = alRight
          Alignment = taRightJustify
          AutoSize = False
          Caption = '2023-04-29'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitLeft = 323
        end
      end
      object Panel42: TPanel
        AlignWithMargins = True
        Left = 21
        Top = 125
        Width = 393
        Height = 32
        Margins.Left = 20
        Margins.Top = 0
        Margins.Right = 20
        Margins.Bottom = 0
        Align = alTop
        Color = clNone
        ParentBackground = False
        TabOrder = 2
        object L_324: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 299
          Height = 24
          Align = alClient
          Caption = #21508#33258#28982#26449#26381#21153#24773#20917#25968#25454
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 130
          ExplicitHeight = 19
        end
        object L_325: TLabel
          AlignWithMargins = True
          Left = 309
          Top = 4
          Width = 80
          Height = 24
          Align = alRight
          Alignment = taRightJustify
          AutoSize = False
          Caption = '2023-04-29'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitLeft = 323
        end
      end
      object Panel43: TPanel
        AlignWithMargins = True
        Left = 21
        Top = 61
        Width = 393
        Height = 32
        Margins.Left = 20
        Margins.Top = 0
        Margins.Right = 20
        Margins.Bottom = 0
        Align = alTop
        Color = clNone
        ParentBackground = False
        TabOrder = 3
        object L_320: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 299
          Height = 24
          Align = alClient
          Caption = #21508#33258#28982#26449#26381#21153#24773#20917#25968#25454
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 130
          ExplicitHeight = 19
        end
        object L_321: TLabel
          AlignWithMargins = True
          Left = 309
          Top = 4
          Width = 80
          Height = 24
          Align = alRight
          Alignment = taRightJustify
          AutoSize = False
          Caption = '2023-04-29'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitLeft = 277
          ExplicitTop = 3
        end
      end
    end
    object Panel35: TPanel
      AlignWithMargins = True
      Left = 472
      Top = 4
      Width = 436
      Height = 172
      HelpKeyword = 'dvbox'
      HelpContext = 13
      Margins.Right = 30
      Align = alClient
      Color = clNone
      ParentBackground = False
      TabOrder = 1
      object Panel44: TPanel
        AlignWithMargins = True
        Left = 21
        Top = 26
        Width = 144
        Height = 32
        Hint = '{"radius":"3px","dwstyle":"border-left:double 16px #19CA88;"}'
        Margins.Left = 20
        Margins.Top = 25
        Margins.Right = 270
        Align = alTop
        Alignment = taLeftJustify
        Color = clNone
        ParentBackground = False
        TabOrder = 0
        object Label51: TLabel
          AlignWithMargins = True
          Left = 6
          Top = 1
          Width = 137
          Height = 30
          Margins.Left = 5
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alClient
          Alignment = taCenter
          Caption = #26381#21153#21344#27604
          Color = 13924904
          Font.Charset = ANSI_CHARSET
          Font.Color = 15852561
          Font.Height = -17
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
          Layout = tlCenter
          ExplicitWidth = 68
          ExplicitHeight = 23
        end
      end
      object M_31: TMemo
        AlignWithMargins = True
        Left = 4
        Top = 64
        Width = 428
        Height = 104
        HelpType = htKeyword
        HelpKeyword = 'echarts'
        Align = alClient
        Lines.Strings = (
          'this.value31 =  ['
          '        { value: 40, name: '#39#24352#19996#39' },'
          '        { value: 30, name: '#39#38472#35779#39' },'
          '        { value: 28, name: '#39#24352#35199#39' },'
          '        { value: 20, name: '#39#26611#26519#26725#39' },'
          '        { value: 22, name: '#39#40644#26725#39' }'
          '      ];'
          '//====='
          'option = {'
          '  legend: {'
          '    top: '#39'center'#39','
          '    left:'#39'5%'#39','
          '    orient:'#39'vertical'#39','
          '    textStyle:{'
          '      color:'#39'#fff'#39
          '    }'
          '  },'
          '  '
          '  series: ['
          '    {'
          '      type: '#39'pie'#39','
          '      radius: [10, 35],'
          '      center: ['#39'70%'#39', '#39'40%'#39'],'
          '      roseType: '#39'area'#39','
          '      itemStyle: {'
          '        borderRadius: 0'
          '      },'
          '      data:this.value31'
          '    }'
          '  ]'
          '};')
        ScrollBars = ssBoth
        TabOrder = 1
      end
    end
    object Panel36: TPanel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 435
      Height = 172
      HelpKeyword = 'dvbox'
      HelpContext = 13
      Margins.Right = 30
      Align = alLeft
      Color = clNone
      ParentBackground = False
      TabOrder = 2
      object Panel33: TPanel
        AlignWithMargins = True
        Left = 21
        Top = 26
        Width = 143
        Height = 32
        Hint = '{"radius":"3px","dwstyle":"border-left:double 16px #19CA88;"}'
        Margins.Left = 20
        Margins.Top = 25
        Margins.Right = 270
        Align = alTop
        Alignment = taLeftJustify
        Color = clNone
        ParentBackground = False
        TabOrder = 0
        object Label37: TLabel
          AlignWithMargins = True
          Left = 6
          Top = 1
          Width = 136
          Height = 30
          Margins.Left = 5
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alClient
          Alignment = taCenter
          Caption = #26381#21153#26032#38395
          Color = 13924904
          Font.Charset = ANSI_CHARSET
          Font.Color = 15852561
          Font.Height = -17
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
          Layout = tlCenter
          ExplicitWidth = 68
          ExplicitHeight = 23
        end
      end
      object Panel37: TPanel
        AlignWithMargins = True
        Left = 21
        Top = 125
        Width = 393
        Height = 32
        Margins.Left = 20
        Margins.Top = 0
        Margins.Right = 20
        Margins.Bottom = 0
        Align = alTop
        Color = clNone
        ParentBackground = False
        TabOrder = 1
        object L_304: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 299
          Height = 24
          Align = alClient
          Caption = #21508#33258#28982#26449#26381#21153#24773#20917#25968#25454
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 130
          ExplicitHeight = 19
        end
        object L_305: TLabel
          AlignWithMargins = True
          Left = 309
          Top = 4
          Width = 80
          Height = 24
          Align = alRight
          Alignment = taRightJustify
          AutoSize = False
          Caption = '2023-04-29'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitTop = 3
        end
      end
      object Panel38: TPanel
        AlignWithMargins = True
        Left = 21
        Top = 93
        Width = 393
        Height = 32
        Margins.Left = 20
        Margins.Top = 0
        Margins.Right = 20
        Margins.Bottom = 0
        Align = alTop
        Color = clNone
        ParentBackground = False
        TabOrder = 2
        object L_302: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 299
          Height = 24
          Align = alClient
          Caption = #21508#33258#28982#26449#26381#21153#24773#20917#25968#25454
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 130
          ExplicitHeight = 19
        end
        object L_303: TLabel
          AlignWithMargins = True
          Left = 309
          Top = 4
          Width = 80
          Height = 24
          Align = alRight
          Alignment = taRightJustify
          AutoSize = False
          Caption = '2023-04-29'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitLeft = 323
        end
      end
      object Panel39: TPanel
        AlignWithMargins = True
        Left = 21
        Top = 61
        Width = 393
        Height = 32
        Margins.Left = 20
        Margins.Top = 0
        Margins.Right = 20
        Margins.Bottom = 0
        Align = alTop
        Color = clNone
        ParentBackground = False
        TabOrder = 3
        object L_300: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 299
          Height = 24
          Align = alClient
          Caption = #21508#33258#28982#26449#26381#21153#24773#20917#25968#25454
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 130
          ExplicitHeight = 19
        end
        object L_301: TLabel
          AlignWithMargins = True
          Left = 309
          Top = 4
          Width = 80
          Height = 24
          Align = alRight
          Alignment = taRightJustify
          AutoSize = False
          Caption = '2023-04-29'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitTop = 3
        end
      end
    end
  end
  object Panel45: TPanel
    AlignWithMargins = True
    Left = 30
    Top = 586
    Width = 1380
    Height = 211
    HelpType = htKeyword
    HelpContext = 5
    Margins.Left = 30
    Margins.Top = 0
    Margins.Right = 30
    Align = alClient
    Color = clNone
    ParentBackground = False
    TabOrder = 4
    object Panel46: TPanel
      AlignWithMargins = True
      Left = 4
      Top = 1
      Width = 300
      Height = 206
      Hint = '{"dwstyle":"border:solid 2px #178BA5;","radius":"3px"}'
      HelpType = htKeyword
      HelpContext = 5
      Margins.Top = 0
      Margins.Right = 20
      Align = alLeft
      Color = clNone
      ParentBackground = False
      TabOrder = 0
      object Panel47: TPanel
        Left = 1
        Top = 1
        Width = 298
        Height = 32
        Hint = '{"dwstyle":"border-bottom:solid 1px #178BA5;"}'
        Align = alTop
        Color = 6501661
        ParentBackground = False
        TabOrder = 0
        object Label52: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 290
          Height = 22
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          Caption = #21508#33258#28982#26449#26381#21153#21344#27604
          Font.Charset = ANSI_CHARSET
          Font.Color = 15852561
          Font.Height = -17
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 136
          ExplicitHeight = 23
        end
      end
      object M_40: TMemo
        AlignWithMargins = True
        Left = 4
        Top = 33
        Width = 292
        Height = 169
        HelpType = htKeyword
        HelpKeyword = 'echarts'
        Margins.Top = 0
        Align = alClient
        Lines.Strings = (
          'this.value40 = [60, 5, 0.3, -100, 1500];'
          '//====='
          'option = {'
          '   grid: {'
          '    top: '#39'15%'#39','
          '    left: '#39'10%'#39','
          '    right: '#39'10%'#39','
          '    bottom: '#39'5%'#39
          '  },'
          ' color: ['#39'#67F9D8'#39', '#39'#FFE434'#39', '#39'#56A3F1'#39', '#39'#FF917C'#39'],'
          '  radar: ['
          '    {'
          '      indicator: ['
          '        { text: '#39#22823#33829#39' },'
          '        { text: '#39#24247#28286#39' },'
          '        { text: '#39#24352#19996#39' },'
          '        { text: '#39#37101#27827#39' },'
          '        { text: '#39#38472#27827#39' }'
          '      ],'
          '      center: ['#39'50%'#39', '#39'55%'#39'],'
          '      startAngle: 90,'
          '      splitNumber: 4,'
          '      axisName: {'
          '        formatter: '#39'{value}'#39','
          '        color: '#39'#fff'#39
          '      },'
          '      splitArea: {'
          '        areaStyle: {'
          '          color: ['#39'#77EADF'#39', '#39'#26C3BE'#39', '#39'#64AFE9'#39', '#39'#428BD4'#39'],'
          '          shadowColor: '#39'rgba(0, 0, 0, 0.2)'#39','
          '          shadowBlur: 10'
          '        }'
          '      },'
          '      axisLine: {'
          '        lineStyle: {'
          '          color: '#39'rgba(211, 253, 250, 0.8)'#39
          '        }'
          '      },'
          '      splitLine: {'
          '        lineStyle: {'
          '          color: '#39'rgba(211, 253, 250, 0.8)'#39
          '        }'
          '      }'
          '    }'
          '  ],'
          '  series: ['
          '    {'
          '      type: '#39'radar'#39','
          '      emphasis: {'
          '        lineStyle: {'
          '          width: 4'
          '        }'
          '      },'
          '      data: ['
          '        {'
          '          value: this.value40,'
          '          areaStyle: {'
          '            color: '#39'rgba(255, 228, 52, 0.6)'#39
          '          }'
          '        }'
          '      ]'
          '    }'
          '  ]'
          '};')
        ScrollBars = ssBoth
        TabOrder = 1
      end
    end
    object Panel52: TPanel
      Left = 324
      Top = 1
      Width = 749
      Height = 209
      HelpType = htKeyword
      HelpContext = 5
      Align = alClient
      Color = clNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 1
      object Panel53: TPanel
        AlignWithMargins = True
        Left = 4
        Top = 4
        Width = 360
        Height = 201
        HelpKeyword = 'dvbox'
        HelpContext = 10
        Margins.Right = 20
        Align = alLeft
        Color = clNone
        ParentBackground = False
        TabOrder = 0
        object M_41: TMemo
          AlignWithMargins = True
          Left = 4
          Top = 11
          Width = 352
          Height = 179
          HelpType = htKeyword
          HelpKeyword = 'echarts'
          Margins.Top = 10
          Margins.Bottom = 10
          Align = alClient
          Lines.Strings = (
            'this.value410 = [23.2, 25.6, 36.7, 35.6, 32.2];'
            'this.value411 = [13.2, 22.6, 31.7, 32, 30];'
            'this.value412 = [13.2, 22.6, 31.7, 32, 30];'
            '//====='
            'option = {'
            '  grid: {'
            '    top: '#39'15%'#39','
            '    left: '#39'10%'#39','
            '    right: '#39'5%'#39','
            '    bottom: '#39'10%'#39
            '  },'
            '  tooltip: {'
            '    trigger: '#39'axis'#39
            '  },'
            '  legend: {'
            '    data: ['#39#20027#21160#26381#21153#39', '#39#34987#21160#26381#21153#39', '#39#38543#26426#26381#21153#39'],'
            '    textStyle: {'
            '      color: '#39'#fff'#39
            '    }'
            '  },'
            '  calculable: true,'
            '  xAxis: ['
            '    {'
            '      type: '#39'category'#39','
            '      data: ['#39#22823#33829#39', '#39#24247#28286#39', '#39#37101#27827#39', '#39#24352#19996#39', '#39#38472#27827#39'],'
            '      axisLabel: {'
            '        textStyle: {'
            '          color: '#39'#fff'#39
            '        }'
            '      }'
            '    }'
            '  ],'
            '  yAxis: ['
            '    {'
            '      type: '#39'value'#39','
            '      axisLabel: {'
            '        textStyle: {'
            '          color: '#39'#fff'#39
            '        }'
            '      }'
            '    }'
            '  ],'
            '  series: ['
            '    {'
            '      name: '#39#20027#21160#26381#21153#39','
            '      type: '#39'bar'#39','
            '      data: this.value410'
            '    },'
            '    {'
            '      name: '#39#34987#21160#26381#21153#39','
            '      type: '#39'bar'#39','
            '      data: this.value411'
            '    },'
            '    {'
            '      name: '#39#38543#26426#26381#21153#39','
            '      type: '#39'bar'#39','
            '      data: this.value412'
            '    }'
            '  ]'
            '};')
          ScrollBars = ssBoth
          TabOrder = 0
        end
      end
      object Panel54: TPanel
        AlignWithMargins = True
        Left = 387
        Top = 4
        Width = 341
        Height = 201
        HelpType = htKeyword
        HelpKeyword = 'dvbox'
        HelpContext = 10
        Margins.Right = 20
        Align = alClient
        Color = clNone
        ParentBackground = False
        TabOrder = 1
        object M_42: TMemo
          AlignWithMargins = True
          Left = 4
          Top = 11
          Width = 333
          Height = 179
          HelpType = htKeyword
          HelpKeyword = 'echarts'
          Margins.Top = 10
          Margins.Bottom = 10
          Align = alClient
          Lines.Strings = (
            'option = {'
            '  grid: {'
            '    top: '#39'15%'#39','
            '    left: '#39'10%'#39','
            '    right: '#39'5%'#39','
            '    bottom: '#39'10%'#39
            '  },'
            '  tooltip: {'
            '    trigger: '#39'axis'#39
            '  },'
            '  legend: {'
            '    data: ['#39#19978#38376#26381#21153#39', '#39#36828#31243#26381#21153#39'],'
            '    textStyle: {'
            '      color: '#39'#fff'#39
            '    }'
            '  },'
            '  calculable: true,'
            '  xAxis: ['
            '    {'
            '      type: '#39'category'#39','
            '      data: ['#39#22823#33829#39', '#39#24247#28286#39', '#39#37101#27827#39', '#39#24352#19996#39', '#39#38472#27827#39'],'
            '      axisLabel: {'
            '        textStyle: {'
            '          color: '#39'#fff'#39
            '        }'
            '      }'
            '    }'
            '  ],'
            '  yAxis: ['
            '    {'
            '      type: '#39'value'#39','
            '      axisLabel: {'
            '        textStyle: {'
            '          color: '#39'#fff'#39
            '        }'
            '      }'
            '    }'
            '  ],'
            '  series: ['
            '    {'
            '      name: '#39#19978#38376#26381#21153#39','
            '      type: '#39'bar'#39','
            '      data: [23.2, 15.6, 16.7, 25.6, 22.2]'
            '    },'
            '    {'
            '      name: '#39#36828#31243#26381#21153#39','
            '      type: '#39'bar'#39','
            '      data: [13.2, 12.6, 11.7, 22, 10]'
            '    }'
            '  ]'
            '};')
          ScrollBars = ssBoth
          TabOrder = 0
        end
      end
    end
    object Panel48: TPanel
      AlignWithMargins = True
      Left = 1076
      Top = 1
      Width = 300
      Height = 206
      Hint = '{"dwstyle":"border:solid 2px #178BA5;","radius":"3px"}'
      HelpType = htKeyword
      HelpContext = 5
      Margins.Top = 0
      Align = alRight
      Color = clNone
      ParentBackground = False
      TabOrder = 2
      object Panel49: TPanel
        Left = 1
        Top = 1
        Width = 298
        Height = 32
        Hint = '{"dwstyle":"border-bottom:solid 1px #178BA5;"}'
        Align = alTop
        Color = 6501661
        ParentBackground = False
        TabOrder = 0
        object Label53: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 290
          Height = 22
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          Caption = #21508#31867#26381#21153#20844#24320#25968#37327
          Font.Charset = ANSI_CHARSET
          Font.Color = 15852561
          Font.Height = -17
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 136
          ExplicitHeight = 23
        end
      end
      object M_43: TMemo
        AlignWithMargins = True
        Left = 4
        Top = 48
        Width = 292
        Height = 154
        HelpType = htKeyword
        HelpKeyword = 'echarts'
        Margins.Top = 15
        Align = alClient
        Lines.Strings = (
          'this.value430 =  [320, 302, 301, 334, 390];'
          'this.value431 =  [120, 132, 101, 134, 90];'
          'this.value432 = [220, 182, 191, 234, 290];'
          'this.value433 = [150, 212, 201, 154, 190];'
          '//====='
          'option = {'
          '  grid: {'
          '    top: '#39'3%'#39','
          '    left: '#39'3%'#39','
          '    right: '#39'3%'#39','
          '    bottom: '#39'3%'#39','
          '    containLabel: true'
          '  },'
          '  xAxis: {'
          '    show:false,'
          '  },'
          '  yAxis: {'
          '    type: '#39'category'#39','
          '    data: ['#39#22823#33829#39', '#39#24247#28286#39', '#39#37101#27827#39', '#39#38472#35779#39', '#39#24352#19996#39'],'
          '    axisLabel:{'
          '        show: true,'
          '        textStyle:{'
          '          color:"#fff"'
          '        }'
          '    }'
          '  },'
          '  series: ['
          '    {'
          '      type: '#39'bar'#39','
          '      stack: '#39'total'#39','
          '      label: {'
          '        show: true,'
          '        textStyle:{'
          '          color:"#fff"'
          '        }'
          '      },'
          '      data:this.value430'
          '    },'
          '    {'
          '      type: '#39'bar'#39','
          '      stack: '#39'total'#39','
          '      label: {'
          '        show: true'
          '      },'
          '      emphasis: {'
          '        focus: '#39'series'#39
          '      },'
          '      data:this.value431'
          '    },'
          '    {'
          '      type: '#39'bar'#39','
          '      stack: '#39'total'#39','
          '      label: {'
          '        show: true'
          '      },'
          '      emphasis: {'
          '        focus: '#39'series'#39
          '      },'
          '      data: this.value432'
          '    },'
          '    {'
          '      type: '#39'bar'#39','
          '      stack: '#39'total'#39','
          '      label: {'
          '        show: true'
          '      },'
          '      emphasis: {'
          '        focus: '#39'series'#39
          '      },'
          '      data: this.value433'
          '    }'
          '  ]'
          '};')
        ScrollBars = ssBoth
        TabOrder = 1
        ExplicitLeft = 3
        ExplicitTop = 47
      end
    end
  end
  object Timer1: TTimer
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 331
    Top = 387
  end
end
