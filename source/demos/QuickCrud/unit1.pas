﻿unit unit1;

interface

uses
    //
    dwBase,
    dwQuickCrud,

    //
    SynCommons,


    //
    Math,Variants,
    Graphics,strutils,
    ComObj,
    Winapi.Windows, Winapi.Messages, Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, System.Classes,
    SysUtils,Vcl.ExtCtrls, Vcl.Grids, Vcl.Buttons,  Vcl.ComCtrls, Vcl.Menus,
    Data.DB, Vcl.DBGrids, Data.Win.ADODB, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Phys.ODBCDef, FireDAC.Phys.MSAccDef, FireDAC.Phys.MSAcc,
  FireDAC.Phys.ODBCBase, FireDAC.Phys.ODBC, FireDAC.Comp.Client, FireDAC.Comp.DataSet,
  Vcl.Imaging.pngimage, FireDAC.Phys.MSSQLDef, FireDAC.Phys.MSSQL;

type
  TForm1 = class(TForm)
    FDConnection1: TFDConnection;
    FDPhysODBCDriverLink1: TFDPhysODBCDriverLink;
    FDPhysMSAccessDriverLink1: TFDPhysMSAccessDriverLink;
    Panel1: TPanel;
    Label1: TLabel;
    Image1: TImage;
    Button1: TButton;
    FDPhysMSSQLDriverLink1: TFDPhysMSSQLDriverLink;
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormDockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
  private
    { Private declarations }
  public
    //QuickCrud 必须的变量
    qcConfig : string;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
    TButton(FindComponent('B_New')).Enabled  := False;
end;

procedure TForm1.FormDockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
begin
    //数据表记录切换事件
    if X = 0 then begin
        dwMessage('数据表记录切换事件！ 主表, 数据记录 : '+IntToStr(Y),'success',self);
    end else begin
        dwMessage('数据表记录切换事件！ 从表 '+IntToStr(X)+', 数据记录 : '+IntToStr(Y),'info',self);
    end;

end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    dwSetPCMode(Self);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
    qcConfig    := StyleName;
    dwCrud(self,FDConnection1,False,'');
end;

end.
