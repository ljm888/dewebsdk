object Form_DBJson: TForm_DBJson
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = #25968#25454#31649#29702
  ClientHeight = 1000
  ClientWidth = 1093
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = 9868950
  Font.Height = -15
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  OnStartDock = FormStartDock
  DesignSize = (
    1093
    1000)
  PixelsPerInch = 96
  TextHeight = 20
  object SG_Main: TStringGrid
    Left = 0
    Top = 40
    Width = 1093
    Height = 209
    Hint = '{"dwattr":"stripe border"}'
    Align = alTop
    BorderStyle = bsNone
    TabOrder = 5
    OnClick = SG_MainClick
    OnGetEditMask = SG_MainGetEditMask
  end
  object P_Banner: TPanel
    Left = 0
    Top = 0
    Width = 1093
    Height = 40
    Align = alTop
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object B_New: TButton
      AlignWithMargins = True
      Left = 898
      Top = 4
      Width = 60
      Height = 30
      Hint = '{"type":"primary","icon":"el-icon-circle-plus-outline"}'
      Margins.Left = 0
      Margins.Top = 4
      Margins.Right = 1
      Margins.Bottom = 6
      Align = alRight
      Caption = #26032#22686
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = B_NewClick
    end
    object E_Keyword: TEdit
      AlignWithMargins = True
      Left = 20
      Top = 6
      Width = 271
      Height = 25
      Hint = 
        '{"placeholder":"'#35831#36755#20837#26597#35810#20851#38190#23383'","radius":"15px","suffix-icon":"el-icon' +
        '-search","dwstyle":"padding-left:10px;"}'
      Margins.Left = 20
      Margins.Top = 6
      Margins.Right = 1
      Margins.Bottom = 9
      Align = alLeft
      TabOrder = 1
      OnChange = E_KeywordChange
      ExplicitHeight = 28
    end
    object B_Print: TButton
      AlignWithMargins = True
      Left = 1020
      Top = 4
      Width = 60
      Height = 30
      Hint = '{"type":"primary","icon":"el-icon-printer"}'
      Margins.Left = 0
      Margins.Top = 4
      Margins.Right = 13
      Margins.Bottom = 6
      Align = alRight
      Caption = #25171#21360
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = B_PrintClick
    end
    object B_Edit: TButton
      AlignWithMargins = True
      Left = 837
      Top = 4
      Width = 60
      Height = 30
      Hint = '{"type":"primary","icon":"el-icon-edit-outline"}'
      Margins.Left = 0
      Margins.Top = 4
      Margins.Right = 1
      Margins.Bottom = 6
      Align = alRight
      Caption = #32534#36753
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = B_EditClick
    end
    object B_Delete: TButton
      AlignWithMargins = True
      Left = 959
      Top = 4
      Width = 60
      Height = 30
      Hint = '{"type":"primary","icon":"el-icon-delete"}'
      Margins.Left = 0
      Margins.Top = 4
      Margins.Right = 1
      Margins.Bottom = 6
      Align = alRight
      Caption = #21024#38500
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = B_DeleteClick
    end
  end
  object PC_Slave: TPageControl
    AlignWithMargins = True
    Left = 0
    Top = 289
    Width = 1093
    Height = 711
    Hint = '{"dwstyle":"border-top:solid 1px #dcdfe6;"}'
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    OnChange = PC_SlaveChange
    object TabSheet1: TTabSheet
      Caption = #35814#32454#21442#25968
      ImageIndex = 56
      object SG_Slave: TStringGrid
        Left = 0
        Top = 0
        Width = 1085
        Height = 209
        Hint = '{"dwattr":"stripe border"}'
        Align = alTop
        BorderStyle = bsNone
        TabOrder = 0
        OnClick = SG_SlaveClick
        OnGetEditMask = SG_MainGetEditMask
      end
    end
  end
  object P_TrackBar: TPanel
    Left = 0
    Top = 249
    Width = 1093
    Height = 40
    Align = alTop
    BevelOuter = bvNone
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 9868950
    Font.Height = -13
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    object TB_Main: TTrackBar
      AlignWithMargins = True
      Left = 5
      Top = 5
      Width = 1088
      Height = 35
      Hint = '{"dwattr":"background"}'
      HelpType = htKeyword
      HelpKeyword = 'page'
      Margins.Left = 5
      Margins.Top = 5
      Margins.Right = 0
      Margins.Bottom = 0
      Align = alClient
      PageSize = 10
      TabOrder = 0
      OnChange = TB_MainChange
    end
  end
  object P_SlaveButtons: TPanel
    Left = 819
    Top = 302
    Width = 256
    Height = 34
    Hint = '{"dwstyle":"z-index:1;"}'
    Margins.Right = 0
    Anchors = [akTop, akRight]
    BevelKind = bkFlat
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 3
    object B_SlaveNew: TButton
      AlignWithMargins = True
      Left = 69
      Top = 0
      Width = 60
      Height = 30
      Hint = '{"type":"primary","icon":"el-icon-circle-plus-outline"}'
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 1
      Margins.Bottom = 0
      Align = alRight
      Caption = #26032#22686
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = B_SlaveNewClick
    end
    object B_SlaveDelete: TButton
      AlignWithMargins = True
      Left = 130
      Top = 0
      Width = 60
      Height = 30
      Hint = '{"type":"primary","icon":"el-icon-delete"}'
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 1
      Margins.Bottom = 0
      Align = alRight
      Caption = #21024#38500
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = B_SlaveDeleteClick
    end
    object B_SlaveEdit: TButton
      AlignWithMargins = True
      Left = 8
      Top = 0
      Width = 60
      Height = 30
      Hint = '{"type":"primary","icon":"el-icon-edit-outline"}'
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 1
      Margins.Bottom = 0
      Align = alRight
      Caption = #32534#36753
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = B_SlaveEditClick
    end
    object B_SlavePrint: TButton
      AlignWithMargins = True
      Left = 191
      Top = 0
      Width = 60
      Height = 30
      Hint = '{"type":"primary","icon":"el-icon-printer"}'
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 1
      Margins.Bottom = 0
      Align = alRight
      Caption = #25171#21360
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = B_SlavePrintClick
    end
  end
  object P_Editor: TPanel
    Left = 304
    Top = 64
    Width = 400
    Height = 569
    Hint = '{"radius":"15px"}'
    HelpType = htKeyword
    HelpKeyword = 'modal'
    BevelOuter = bvNone
    Caption = 'P_Editor'
    ParentBackground = False
    TabOrder = 4
    Visible = False
    object P_EditorButtons: TPanel
      Left = 0
      Top = 519
      Width = 400
      Height = 50
      Hint = '{"dwstyle":"border-top:solid 1px #eee;"}'
      Align = alBottom
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object Button_Cancel: TButton
        Left = 200
        Top = 0
        Width = 200
        Height = 50
        Hint = 
          '{"dwstyle":"background:#FFF;border:solid 1px #dcdfd6;","radius":' +
          '"0px"}'
        Margins.Top = 10
        Margins.Right = 15
        Margins.Bottom = 10
        Align = alRight
        Caption = #21462#28040
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = Button_CancelClick
      end
      object Button_OK: TButton
        Left = 0
        Top = 0
        Width = 200
        Height = 50
        Hint = 
          '{"dwstyle":"background:#fff;border-top:solid 1px #dcdfd6;border-' +
          'right:0px;","radius":"0px"}'
        Margins.Top = 10
        Margins.Bottom = 10
        Align = alClient
        Caption = #30830#23450
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = Button_OKClick
      end
    end
    object SB_Demo: TScrollBox
      AlignWithMargins = True
      Left = 0
      Top = 60
      Width = 400
      Height = 459
      Hint = '{"dwstyle":"border-top:solid 1px #eee;"}'
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      VertScrollBar.Visible = False
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 1
      Visible = False
      object Panel_Content: TPanel
        Left = 0
        Top = 0
        Width = 400
        Height = 240
        Align = alTop
        AutoSize = True
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel_string: TPanel
          AlignWithMargins = True
          Left = 10
          Top = 120
          Width = 387
          Height = 40
          Margins.Left = 10
          Margins.Top = 0
          Margins.Bottom = 0
          Align = alTop
          BevelKind = bkTile
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          Visible = False
          object Label1: TLabel
            AlignWithMargins = True
            Left = 10
            Top = 0
            Width = 80
            Height = 36
            Margins.Left = 10
            Margins.Top = 0
            Margins.Right = 10
            Margins.Bottom = 0
            Align = alLeft
            AutoSize = False
            Caption = #21517#31216
            Layout = tlCenter
            ExplicitLeft = 20
            ExplicitHeight = 49
          end
          object Edit1: TEdit
            AlignWithMargins = True
            Left = 100
            Top = 6
            Width = 253
            Height = 30
            Hint = 
              '{"dwstyle":"border:0;border-bottom:solid 1px #eee;border-radius:' +
              '0px;"}'
            Margins.Left = 0
            Margins.Top = 6
            Margins.Right = 30
            Margins.Bottom = 0
            Align = alClient
            TabOrder = 0
            ExplicitHeight = 28
          end
        end
        object Panel_combo: TPanel
          AlignWithMargins = True
          Left = 10
          Top = 0
          Width = 387
          Height = 40
          Margins.Left = 10
          Margins.Top = 0
          Margins.Bottom = 0
          Align = alTop
          BevelKind = bkTile
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          Visible = False
          object Label2: TLabel
            AlignWithMargins = True
            Left = 10
            Top = 0
            Width = 80
            Height = 36
            Margins.Left = 10
            Margins.Top = 0
            Margins.Right = 10
            Margins.Bottom = 0
            Align = alLeft
            AutoSize = False
            Caption = #22320#22336
            Layout = tlCenter
            ExplicitTop = -1
          end
          object ComboBox1: TComboBox
            AlignWithMargins = True
            Left = 100
            Top = 6
            Width = 253
            Height = 29
            Hint = 
              '{"dwstyle":"border:0;border-bottom:solid 1px #eee;border-radius:' +
              '0px;"}'
            Margins.Left = 0
            Margins.Top = 6
            Margins.Right = 30
            Margins.Bottom = 0
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 9868950
            Font.Height = -16
            Font.Name = #24494#36719#38597#40657
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
        end
        object Panel_integer: TPanel
          AlignWithMargins = True
          Left = 10
          Top = 80
          Width = 387
          Height = 40
          Margins.Left = 10
          Margins.Top = 0
          Margins.Bottom = 0
          Align = alTop
          BevelKind = bkTile
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 2
          Visible = False
          object Label3: TLabel
            AlignWithMargins = True
            Left = 10
            Top = 0
            Width = 80
            Height = 36
            Margins.Left = 10
            Margins.Top = 0
            Margins.Right = 10
            Margins.Bottom = 0
            Align = alLeft
            AutoSize = False
            Caption = #21517#31216
            Layout = tlCenter
            ExplicitLeft = 20
            ExplicitHeight = 49
          end
          object SpinEdit1: TSpinEdit
            AlignWithMargins = True
            Left = 100
            Top = 6
            Width = 253
            Height = 30
            Hint = '{"dwstyle":"border:solid 1px #eee;"}'
            Margins.Left = 0
            Margins.Top = 6
            Margins.Right = 30
            Margins.Bottom = 0
            Align = alClient
            MaxValue = 0
            MinValue = 0
            TabOrder = 0
            Value = 0
          end
        end
        object Panel_date: TPanel
          AlignWithMargins = True
          Left = 10
          Top = 40
          Width = 387
          Height = 40
          Margins.Left = 10
          Margins.Top = 0
          Margins.Bottom = 0
          Align = alTop
          BevelKind = bkTile
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 3
          Visible = False
          object Label4: TLabel
            AlignWithMargins = True
            Left = 10
            Top = 0
            Width = 80
            Height = 36
            Margins.Left = 10
            Margins.Top = 0
            Margins.Right = 10
            Margins.Bottom = 0
            Align = alLeft
            AutoSize = False
            Caption = #21517#31216
            Layout = tlCenter
            ExplicitLeft = 20
            ExplicitHeight = 49
          end
          object DateTimePicker1: TDateTimePicker
            AlignWithMargins = True
            Left = 100
            Top = 6
            Width = 253
            Height = 30
            Hint = 
              '{"dwstyle":"border:0;border-bottom:solid 1px #eee;border-radius:' +
              '0px;"}'
            Margins.Left = 0
            Margins.Top = 6
            Margins.Right = 30
            Margins.Bottom = 0
            Align = alClient
            Date = 44670.000000000000000000
            Time = 0.955161249999946400
            TabOrder = 0
          end
        end
        object Panel_Boolean: TPanel
          AlignWithMargins = True
          Left = 10
          Top = 160
          Width = 387
          Height = 40
          Margins.Left = 10
          Margins.Top = 0
          Margins.Bottom = 0
          Align = alTop
          BevelKind = bkTile
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 4
          Visible = False
          object Label5: TLabel
            AlignWithMargins = True
            Left = 10
            Top = 0
            Width = 80
            Height = 36
            Margins.Left = 10
            Margins.Top = 0
            Margins.Right = 10
            Margins.Bottom = 0
            Align = alLeft
            AutoSize = False
            Caption = #21517#31216
            Layout = tlCenter
            ExplicitLeft = 20
            ExplicitHeight = 49
          end
          object ToggleSwitch1: TToggleSwitch
            AlignWithMargins = True
            Left = 100
            Top = 6
            Width = 253
            Height = 30
            Hint = '{"dwstyle":"border-bottom:solid 1px #eee;"}'
            Margins.Left = 0
            Margins.Top = 6
            Margins.Right = 30
            Margins.Bottom = 0
            Align = alClient
            AutoSize = False
            ShowStateCaption = False
            StateCaptions.CaptionOn = 'true'
            StateCaptions.CaptionOff = 'false'
            TabOrder = 0
            ThumbColor = clHotLight
          end
        end
        object Panel_Group: TPanel
          AlignWithMargins = True
          Left = 10
          Top = 200
          Width = 387
          Height = 40
          Hint = '{"dwstyle":"border-bottom:solid 1px #eee;"}'
          Margins.Left = 10
          Margins.Top = 0
          Margins.Bottom = 0
          Align = alTop
          BevelKind = bkTile
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 5
          Visible = False
          object Label6: TLabel
            AlignWithMargins = True
            Left = 30
            Top = 15
            Width = 343
            Height = 21
            Margins.Left = 30
            Margins.Top = 15
            Margins.Right = 10
            Margins.Bottom = 0
            Align = alClient
            AutoSize = False
            Caption = 'Group'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 9868950
            Font.Height = -15
            Font.Name = #24494#36719#38597#40657
            Font.Style = [fsBold]
            ParentFont = False
            Layout = tlCenter
            ExplicitLeft = 10
            ExplicitTop = 0
            ExplicitWidth = 199
            ExplicitHeight = 36
          end
        end
      end
    end
    object P_EditorTitle: TPanel
      AlignWithMargins = True
      Left = 0
      Top = 0
      Width = 400
      Height = 50
      Hint = '{"dwstyle":"border-bottom:solid 1px #eee;"}'
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 10
      Align = alTop
      BevelOuter = bvNone
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 2
      object Label7: TLabel
        AlignWithMargins = True
        Left = 10
        Top = 0
        Width = 380
        Height = 50
        Margins.Left = 10
        Margins.Top = 0
        Margins.Right = 10
        Margins.Bottom = 0
        Align = alClient
        Alignment = taCenter
        AutoSize = False
        Caption = #25968#25454#32534#36753
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
        ExplicitLeft = 18
        ExplicitWidth = 80
        ExplicitHeight = 46
      end
    end
  end
  object FDQuery1: TFDQuery
    Left = 90
    Top = 281
  end
end
