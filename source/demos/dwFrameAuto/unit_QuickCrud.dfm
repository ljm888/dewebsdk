object Form_QuickCrud: TForm_QuickCrud
  Left = 0
  Top = 0
  HelpType = htKeyword
  Margins.Top = 20
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = #24555#36895'CRUD'
  ClientHeight = 468
  ClientWidth = 933
  Color = clWindow
  TransparentColor = True
  TransparentColorValue = clWhite
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -17
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  StyleName = 
    '{"table":"dwf_goods","pagesize":5,"rowheight":40,"fields":[{"nam' +
    'e":"id","caption":"id","width":50,"align":"center","type":"auto"' +
    '},{"name":"goodsname","caption":"'#36135#21697#21517#31216'","width":180,"type":"strin' +
    'g","must":1,"sort":1},{"name":"goodscode","caption":"'#32534#30721'","width"' +
    ':120,"must":1,"sort":1},{"name":"provider","caption":"'#20379#24212#21830'","type' +
    '":"combo","list":["'#20013#20852'","'#21326#20026'","'#36808#21326'","'#32511#30005'"],"width":180},{"name":"spe' +
    'c","caption":"'#35268#26684'","type":"string","width":150},{"name":"unit","t' +
    'ype":"combo","caption":"'#21333#20301'","list":["","'#37096'","'#20010'","'#25903'","'#21488'","'#31859'","'#36742'","' +
    #22871'"],"width":60},{"name":"inprice","caption":"'#36827#20215'","width":80,"ali' +
    'gn":"right","type":"money","sort":1},{"name":"price","caption":"' +
    #21806#20215'","width":80,"type":"money","align":"right","sort":1},{"name":' +
    '"description","caption":"'#22791#27880'","width":150}],"slavepagesize":5,"sl' +
    'averowheight":30,"slave":[{"caption":"'#35814#32454#21442#25968'","table":"dwf_goodsex' +
    '","imageindex":58,"masterfield":"id","slavefield":"gid","edit":1' +
    ',"new":1,"delete":1,"print":0,"fields":[{"name":"id","caption":"' +
    'id","width":80,"align":"center","sort":1,"type":"auto"},{"name":' +
    '"gid","caption":"'#36135#21697'ID","align":"left","must":1,"type":"integer",' +
    '"width":80},{"name":"title","caption":"'#23646#24615#21517#31216'","must":1,"width":12' +
    '0,"must":1},{"name":"value","caption":"'#23646#24615#20540'","width":180,"must":1' +
    '}]},{"caption":"'#36827#36135#35760#24405'","table":"dwf_inport","imageindex":59,"mast' +
    'erfield":"id","slavefield":"gid","edit":0,"new":0,"delete":0,"pr' +
    'int":1,"fields":[{"name":"id","caption":"id","width":80,"sort":1' +
    ',"align":"center","type":"auto"},{"name":"gid","caption":"'#36135#21697'ID",' +
    '"type":"integer","must":1,"width":140},{"name":"provider","capti' +
    'on":"'#20379#24212#21830'","align":"left","must":1,"width":180,"must":1},{"name":' +
    '"unit","caption":"'#21333#20301'","width":60,"type":"combo","list":["","'#37096'","' +
    #20010'","'#25903'","'#21488'","'#31859'","'#36742'","'#22871'"]},{"name":"price","caption":"'#21333#20215'","type":"' +
    'money","width":80},{"name":"num","caption":"'#25968#37327'","width":80,"type' +
    '":"integer","must":1},{"name":"amount","caption":"'#37329#39069'","type":"mo' +
    'ney","width":80},{"name":"ware","caption":"'#20179#24211'","width":120},{"na' +
    'me":"operator","caption":"'#25805#20316#21592'","must":1,"width":100},{"name":"da' +
    'te","caption":"'#26085#26399'","width":120,"type":"date"},{"name":"remark","' +
    'caption":"'#22791#27880'","width":60}]},{"caption":"'#38144#21806#35760#24405'","table":"dwf_outpo' +
    'rt","imageindex":60,"masterfield":"id","slavefield":"gid","edit"' +
    ':0,"new":1,"delete":0,"print":1,"fields":[{"name":"id","caption"' +
    ':"id","width":50,"align":"center","type":"auto"},{"name":"gid","' +
    'caption":"'#36135#21697'ID","type":"integer","width":80},{"name":"unit","cap' +
    'tion":"'#21333#20301'","type":"combo","list":["","'#37096'","'#20010'","'#25903'","'#21488'","'#31859'","'#36742'","'#22871'"' +
    '],"width":80},{"name":"price","caption":"'#21333#20215'","sort":1,"width":80' +
    '},{"name":"num","caption":"'#25968#37327'","width":80,"type":"integer","must' +
    '":1},{"name":"amount","caption":"'#37329#39069'","type":"money","width":100}' +
    ',{"name":"operator","caption":"'#32463#21150#20154'","width":120,"must":1},{"name' +
    '":"date","caption":"'#26085#26399'","width":200,"type":"time"},{"name":"rema' +
    'rk","caption":"'#22791#27880'","width":120}]}]}'
  OnMouseUp = FormMouseUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 23
end
