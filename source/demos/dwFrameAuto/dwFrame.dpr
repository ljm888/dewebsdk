﻿library dwFrame;

uses
  ShareMem,
  SysUtils,
  Forms,
  Messages,
  StdCtrls,
  Variants,
  Windows,
  Classes,
  Data.Win.ADODB,
  unit1 in 'unit1.pas' {Form1},
  {__uses__}
  unit_Card in 'unit_Card.pas' {Form_Card},
  unit_DB in 'unit_DB.pas' {Form_DB},
  unit_DBJson in 'unit_DBJson.pas' {Form_DBJson},
  unit_Document in 'unit_Document.pas' {Form_Document},
  unit_Hello in 'unit_Hello.pas' {Form_Hello},
  unit_Inventory in 'unit_Inventory.pas' {Form_Inventory},
  unit_Product in 'unit_Product.pas' {Form_Product},
  unit_QuickCrud in 'unit_QuickCrud.pas' {Form_QuickCrud},
  unit_Requisition in 'unit_Requisition.pas' {Form_Requisition},
  unit_Role in 'unit_Role.pas' {Form_Role},
  unit_Stat in 'unit_Stat.pas' {Form_Stat},
  unit_StockIn in 'unit_StockIn.pas' {Form_StockIn},
  unit_StockInQuery in 'unit_StockInQuery.pas' {Form_StockInQuery},
  unit_Supplier in 'unit_Supplier.pas' {Form_Supplier},
  unit_User in 'unit_User.pas' {Form_User},
  unit_WareHouse in 'unit_WareHouse.pas' {Form_WareHouse},
  unit_Home in 'unit_Home.pas' {Form_Home};

{$R *.res}

var
    DLLApp      : TApplication;
    DLLScreen   : TScreen;


function dwLoad(AParams:String;AConnection:Pointer;AApp:TApplication;AScreen:TScreen):TForm;stdcall;
var
    sDir        : string;
begin
    //赋值主程序参数
    Application := AApp;
    Screen      := AScreen;

    //创建应用主窗体
    Form1               := TForm1.Create(nil);
    //取得运行目录备用
    Form1.gsMainDir     := ExtractFilePath(Application.ExeName);
    //设置数据库连接
    Form1.FDConnection1.SharedCliHandle  := AConnection;


    //返回值
    Result      := Form1;
end;

procedure DLLUnloadProc(dwReason: DWORD);
begin
    if dwReason = DLL_PROCESS_DETACH then begin
        Application    := DLLApp;   //恢复
        Screen         := DLLScreen;
    end;
end;



exports
    dwLoad;

begin
    DLLApp    := Application;       //保存 DLL 中初始的 Application 对象
    DLLScreen := Screen;
    DLLProc   := @DLLUnloadProc;    //保证 DLL 卸载时恢复原来的 Application
    DLLUnloadProc(DLL_PROCESS_DETACH);
end.
