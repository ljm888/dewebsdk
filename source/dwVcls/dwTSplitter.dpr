﻿library dwTSplitter;

uses
     ShareMem,

     //
     dwCtrlBase,

     //
     SynCommons,

     //
     Messages, SysUtils, Variants, Classes, Graphics,
     Controls, Forms, Dialogs, ComCtrls, ExtCtrls,
     StdCtrls, Windows;


function _GetCursor(ACtrl:TComponent):String;
begin
    case TSplitter(ACtrl).Align of
        alLeft,alRight : begin
            Result  := 'cursor:e-resize;'
        end;
        alTop,alBottom : begin
            Result  := 'cursor:s-resize;'
        end;
    end;
end;





//==================================================================================================

//当前控件需要引入的第三方JS/CSS
function dwGetExtra(ACtrl:TComponent):string;stdCall;
begin
     Result    := '[]';
end;



//根据JSON对象AData执行当前控件的事件, 并返回结果字符串
function dwGetEvent(ACtrl:TComponent;AData:String):string;StdCall;
var
     joData    : Variant;
begin
    with TSplitter(ACtrl) do begin
        //
        joData    := _Json(AData);

        if joData.e = 'onclick' then begin
        end;
    end;
end;


//取得HTML头部消息
function dwGetHead(ACtrl:TComponent):string;StdCall;
var
    sCode       : string;
    sFull       : string;
    joHint      : Variant;
    joRes       : Variant;
begin
    //
    sFull   := dwFullName(Actrl);

    //
    with TSplitter(ACtrl) do begin

        //生成返回值数组
        joRes    := _Json('[]');

        //取得HINT对象JSON
        joHint    := dwGetHintJson(TControl(ACtrl));

        //
        sCode   := '<el-main' +
                ' id="'+sFull+'"' +
                dwVisible(TControl(ACtrl)) +
                dwDisable(TControl(ACtrl)) +
                dwGetDWAttr(joHint) +
                ' :style="{' +
                   'backgroundColor:'+sFull+'__col,' +
                   'left:'+sFull+'__lef,' +
                   'top:'+sFull+'__top,' +
                   'width:'+sFull+'__wid,' +
                   'height:'+sFull+'__hei' +
                '}"'  +
                ' style="' +
                   'position:'+dwIIF((Parent.ControlCount=1)and(Parent.ClassName='TScrollBox'),'relative','absolute')+';' +
                   'overflow:hidden;' +
                   _GetCursor(ACtrl) +
                   dwGetDWStyle(joHint) +
                '"'  +//style 封闭

                ' @mousedown.native="'+sFull+'__mousedown"' +
                ' @mouseup.native="'+sFull+'__mouseup"' +
                ' @mousemove.native="'+sFull+'__mousemove"' +
                ' @mouseout.native="'+sFull+'__mouseout"' +
                '>';
        //添加到返回值数据
        joRes.Add(sCode);
        //
        Result    := (joRes);
    end;
end;

//取得HTML尾部消息
function dwGetTail(ACtrl:TComponent):string;StdCall;
var
    joRes       : Variant;
    sCode       : String;
    sFull       : string;
begin
    //
    sFull   := dwFullName(Actrl);

    with TSplitter(ACtrl) do begin
        //生成返回值数组
        joRes    := _Json('[]');
        //生成返回值数组
        joRes.Add('</el-main>');
        //
        Result    := (joRes);
    end;
end;

//取得Data
function dwGetData(ACtrl:TComponent):string;StdCall;
var
    sFull       : string;
    joRes       : Variant;
    joHint      : Variant;
begin
    //
    sFull   := dwFullName(Actrl);

    with TSplitter(ACtrl) do begin
        //生成返回值数组
        joRes    := _Json('[]');
        //
        with TSplitter(ACtrl) do begin
            joRes.Add(sFull+'__lef:"'+IntToStr(Left)+'px",');
            joRes.Add(sFull+'__top:"'+IntToStr(Top)+'px",');
            joRes.Add(sFull+'__wid:"'+IntToStr(Width)+'px",');
            joRes.Add(sFull+'__hei:"'+IntToStr(Height)+'px",');
            //
            joRes.Add(sFull+'__vis:'+dwIIF(Visible,'true,','false,'));
            joRes.Add(sFull+'__dis:'+dwIIF(Enabled,'false,','true,'));
            //
            if TSplitter(ACtrl).Color = clNone then begin
                joRes.Add(sFull+'__col:"rgba(0,0,0,0)",');
            end else begin
                joRes.Add(sFull+'__col:"'+dwAlphaColor(TPanel(ACtrl))+'",');
            end;
            //
            joRes.Add(sFull+'__rtz:30,');
        end;
        //
        Result    := (joRes);
    end;
end;

function dwGetAction(ACtrl:TComponent):string;StdCall;
var
    sFull   : String;
    joRes   : Variant;
    joHint  : Variant;
begin
    //
    sFull   := dwFullName(Actrl);

    with TSplitter(ACtrl) do begin
        //生成返回值数组
        joRes    := _Json('[]');
        //
        with TSplitter(ACtrl) do begin
            joRes.Add('this.'+sFull+'__lef="'+IntToStr(Left)+'px";');
            joRes.Add('this.'+sFull+'__top="'+IntToStr(Top)+'px";');
            joRes.Add('this.'+sFull+'__wid="'+IntToStr(Width)+'px";');
            joRes.Add('this.'+sFull+'__hei="'+IntToStr(Height)+'px";');
            //
            joRes.Add('this.'+sFull+'__vis='+dwIIF(Visible,'true;','false;'));
            joRes.Add('this.'+sFull+'__dis='+dwIIF(Enabled,'false;','true;'));
            //
            if TSplitter(ACtrl).Color = clNone then begin
                joRes.Add('this.'+sFull+'__col="rgba(0,0,0,0)";');
            end else begin
                joRes.Add('this.'+sFull+'__col="'+dwAlphaColor(TPanel(ACtrl))+'";');
            end;
        end;
        //
        Result    := (joRes);
    end;
end;

function dwGetMethods(ACtrl:TControl):String;stdCall;
var
    //
    sCode   : string;
    sFull   : string;
    //
    joHint  : Variant;
    joRes   : Variant;
begin
    //返回值 JSON对象，可以直接转换为字符串
    joRes   := _json('[]');

    //
    sFull   := dwFullName(Actrl);

    //取得HINT对象JSON
    joHint    := dwGetHintJson(TControl(ACtrl));

    with TButton(ACtrl) do begin
        //函数头部
        sCode   :=
                sFull+'__mousedown(event) {'#13 +
                    'this.dwevent("",'''+Name+''',''0'',''onclick'','''+IntToStr(TForm(Owner).Handle)+''');' +
                '},';

        //
        joRes.Add(sCode);
    end;

    //
    Result   := joRes;
end;


exports
     dwGetExtra,
     dwGetEvent,
     dwGetMethods,
     dwGetHead,
     dwGetTail,
     dwGetAction,
     dwGetData;
     
begin
end.
 
