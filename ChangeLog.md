# 更新日志

## 2023-06-29
DeWeb V2.0.20230627发布

### 新增
* 为dwLoad的参数AParams中增加了name和params， 开发者可以取得更多信息


### 修复
* 基本解决了级联选择器的代码设置问题；
* 完善了多文件上传
  使用dwUploadMultiple函数完成
  第一个文件上传前激活当前窗体的OnStartDock事件；
  所有文件上传完成后激活当前窗体的OnEndDock事件；
  上传完成后，源文件名称位于窗体的Hint中，如：{...,"__uploadsources":["1.jpg","a.exe"]}
  最终文件名称位于窗体的Hint中，如：{...,"__uploadfiles":["20230629_080546163_160.jpg","20230629_080630941_134.exe"]}
* 完善了QuickCrud若干方面
   - 从表新增时自动添加与主表对应的字段
   - 新增时字段支持设置默认值 
   - 保存失败时，提示必填字段名称或不显示名称
   - 新增编辑界面日期控件替换，换成跟查询时控件一致。
   - 字段增加了readonly属性支持。如："readonly":1
   - 修改编辑界面,必填标红
   - 增加/编辑最大化按钮
   - 增加where属性，在配置中可以增加where项，用于只查询部分数据，如："where":"id>100"
   - 增加了datetime字段类型，支持查询
   - 完善了meney字段类型，字段显示时显示为 1,234.00元。 前面千分位显示，后面补2个零
   - 导出EXCEL时默认为caption，而不是fieldname
* 完善了RadioButton

## 2023-06-12
DeWeb V2.0.20230612发布

### 新增
* 支持多账套
  - 开发兼容原模式，如果使用第一个账套，则开发无需要修改
  - 如果需要使用其他账套，可以函数 dwGetCliHandleByID 或 dwGetCliHandleByName 进行配置
```delphi
    //根据 id (序号，0开始) 获取数据库连接
    //Form1.FDConnection1.SharedCliHandle   := dwGetCliHandleByID(AParams,0);//AConnection;

    //根据 Name (不区分大小写) 获取数据库连接
    Form1.FDConnection1.SharedCliHandle   := dwGetCliHandleByName(AParams,'dwAccess');//AConnection;
```
* 增加了Hint悬浮提示，采用TPanel__Hint实现
* 支持多文件上传
* 增加了代码编辑控件Memo__codemirror
* 增加了代码显示控件Memo__highlight
* 增加了大屏看板实现https://www.delphibbs.com/bdv001
* 增加了Echarts地图显示控件Memo__echartsmap，可以绘制地图和响应click事件
* 增加了布局控件支持：TFlowPanel,TGridPanel,TStackPanel和TCardPanel
### 修复
* 修复完善dwQuickCrud, 主要解决了
  - 稳定性，一些情况下报错
  - 删除固定为第一条记录，已解决
  - 增加了integer/money字段的最大最小值
  - 主从表的批量新增
* 修复完善了TScrollBox控件
* 更新了ComboBox，支持OnDropDown和OnCloseUp事件

## 2023-04-23

### 新增
* 开始写更新日志  

### 修复

* 从表新增后，再点主表新增，显示中间空白  
* 新增点OK后，报错，但可以新增记录。   
解决方案：通过post前加一行,解决 
```delphi
FDQuery1.FetchOptions.RecsSkip  := -1;
```

## 2023-04-22

### 修复

* dwTTimer.dpr的几处调试后未恢复的笔误

## 2023-04-21

### 新增
* 增加了TMemo__usescomp控件，以解决子窗体部分控件不能正常显示的bug

### 更新
* 修改了TForm控件，配合DWS更新, 解决子窗体upload后，激活主窗体的StartDock/EndDock事件的问题
  修改后激活对应窗体的 StartDock/EndDock事件的问题
* 更新了TCalendar控件，以显示日历 